# Mumsic

Music bot for Mumble

# Setup

```sh
$ git clone https://gitlab.com/alifurkany/mumsic
$ cd mumsic
$ npm install
```

# Configuration

Configuration is imported from `config.js`. Here are the defaults:

```js
module.export = {
	prefix: "!",
	url: "localhost",
	port: "64738",
	rejectUnauthorized: false,
	name: "NoodleJS",
	password: "",
	tokens: []
};
```

# Usage

```sh
$ npm start
```
