const config = require("./config");
const noodle = require("noodle.js");
const ytsr = require("ytsr");
const ytdl = require("ytdl-core");

if (typeof config.prefix === "undefined") config.prefix = "!";

/**
 * @typedef {import("noodle.js/src/structures/Collection")} Collection
 * @typedef {import("noodle.js/src/structures/User")} User
 * @typedef {import("noodle.js/src/structures/TextMessage")} TextMessage
 * @typedef {import("noodle.js/src/structures/Channel")} Channel
 */

/**
 * @param {TextMessage} message
 * @param {string} content
 */
function reply(message, content) {
	if (message.channels.size > 0)
		return message.channels.entries().next().value[1].sendMessage(content);
	else if (message.users.size > 0) return message.sender.sendMessage(content);
}

/**
 * @param {number} seconds
 * @returns {string}
 */
function secondToTime(seconds) {
	let hours = Math.floor(seconds / 3600);
	seconds %= 3600;
	let minutes = Math.floor(seconds / 60);
	seconds %= 60;
	return (
		(hours ? hours.toString().padStart(2, "0") + ":" : "") +
		minutes.toString().padStart(2, "0") +
		":" +
		seconds.toString().padStart(2, "0")
	);
}

/**
 * @param {string} str
 * @returns {string}
 */
const escapeHtml = (str) =>
	str
		.replace(/&/g, "&amp;")
		.replace(/</g, "&lt;")
		.replace(/>/g, "&gt;")
		.replace(/"/g, "&quot;")
		.replace(/'/g, "&#039;");

const videoTitleWithLink = (video) =>
	`<a href="${video.videoDetails.video_url}">${escapeHtml(
		video.videoDetails.title
	)}</a> (${secondToTime(video.videoDetails.lengthSeconds)})`;

let state = require("./saveToFile")(".state.json", {
	channel: null
});

const client = new noodle(config);

/** @type {ytdl.videoInfo[]} */
let videoQueue = [];

client.on("ready", async (info) => {
	console.log("Joined server");
	console.log("Welcome message: " + info.welcomeMessage);
	if (state.channel !== null) {
		console.log("Joining last channel");
		try {
			await client.switchChannel(state.channel);
		} catch {
			console.log("Failed to join last channel");
			state.channel = null;
		}
	}
	client.sendMessage(
		`Say ${config.prefix}help to see the available commands`
	);
});
client.on("userJoin", (user) => {
	console.log(`${user.name} has joined the server.`);
});
client.on("userDisconnect", (user) => {
	if (user.session === client.session) {
		client.destroy();
		videoQueue = [];
		console.log("Disconnected from server");
	} else console.log(`${user.name} has left the server.`);
});
client.on("userChange", (oldUser, newUser) => {
	if (oldUser.session === client.user.session) {
		if (oldUser.channel.id !== newUser.channel.id) {
			console.log(`Channel changed to ${newUser.channel.name}`);
			client.sendMessage(
				`Joined ${newUser.channel.name} (${newUser.channel.id})`
			);
			state.channel = newUser.channel.id;
		}
	}
});
client.on("message", async (message) => {
	if (message.content.startsWith(config.prefix)) {
		let command = message.content.slice(config.prefix.length).trim();
		let args = [];
		args.push(command.split(" ")[0]);
		args.push(command.slice(args[0].length).trim());

		switch (args[0]) {
			case "help":
				reply(
					message,
					"Commands:<br>" +
						"help - Shows this message<br>" +
						"join <channel name or id> - Joins a channel<br>" +
						"play <url or query> - Plays a YouTube video<br>" +
						"skip - Skips the current video<br>" +
						"stop - Stops playing and clears queue<br>" +
						"clear - Clears the queue<br>" +
						"volume [percent] - Sets or shows the volume<br>" +
						"nowplaying - Information about the current video<br>" +
						"np - Alias for nowplaying<br>" +
						"queue - Shows the current queue<br>" +
						"q - Alias for queue<br>" +
						"loop - Toggles loop<br>" +
						"leave - Stops the bot and leaves the server<br>" +
						"ping - Replies with 'Pong!'"
				);
				break;
			case "join":
				if (!args[1]) {
					reply(message, "Please specify a channel name.");
					break;
				} else {
					// Find channel to join
					let channel =
						client.channels.find(
							(channel) => channel.name === args[1]
						) ||
						client.channels.find(
							(channel) => channel.id === args[1]
						) ||
						client.channels.find(
							(channel) =>
								channel.name.toLowerCase() ===
								args[1].toLowerCase()
						) ||
						client.channels.find((channel) =>
							channel.name
								.toLowerCase()
								.match(args[1].toLowerCase())
						);
					if (!channel) reply(message, "Channel not found.");
					else {
						reply(
							message,
							`Trying to join ${channel.name} (${channel.id})`
						);
						try {
							await client.switchChannel(channel.id);
						} catch {
							reply(message, "Channel not found.");
						}
					}
				}
				break;
			case "play":
				if (!args[1]) {
					reply(message, "Please specify a video name or URL.");
					break;
				}
				// Mumble converts links to <a href="...">...</a>
				if (args[1].startsWith("<a href="))
					args[1] = args[1].split('"')[1];
				if (ytdl.validateURL(args[1])) {
					/** @type {ytdl.videoInfo} */
					let info;
					try {
						info = await ytdl.getInfo(args[1]);
					} catch {
						reply(message, "Video not found.");
						break;
					}
					reply(
						message,
						`Adding ${videoTitleWithLink(info)} to queue.`
					);
					videoQueue.push(info);
					if (!playing) playNext();
				} else {
					// Find video to play
					reply(message, "Searching for video...");
					let video = await ytsr(args[1]);
					if (!video || video.items.length === 0) {
						reply(message, "Video not found.");
						break;
					}
					let info = await ytdl.getInfo(video.items[0].url);
					reply(
						message,
						`Adding ${videoTitleWithLink(info)} to queue.`
					);
					videoQueue.push(info);
					if (!playing) playNext();
				}
				break;
			case "skip":
				if (!nowPlaying) {
					reply(message, "There is nothing to skip.");
					break;
				}
				reply(message, "Skipping...");
				nowPlaying.stream.destroy();
				client.voiceConnection.stopStream();
				nowPlaying = null;
				playNext();
				break;
			case "stop":
				let stopped = false,
					cleared = false;
				if (nowPlaying) {
					nowPlaying.stream.destroy();
					nowPlaying = null;
					playing = false;
					stopped = true;
				}
				if (videoQueue.length > 0) {
					videoQueue = [];
					cleared = true;
				}
				if (!stopped && !cleared) reply(message, "Nothing to do.");
				else
					reply(
						message,
						`${
							stopped ? "Stopped playing and cleared" : "Cleared"
						} the queue.`
					);
				break;
			case "clear":
				if (videoQueue.length > 0) {
					videoQueue = [];
					reply(message, "Cleared the queue.");
				} else reply(message, "The queue is already empty.");
				break;
			case "volume":
				if (!args[1]) {
					reply(
						message,
						`The current volume is ${
							client.voiceConnectio.getVolume() * (100).toFixed(2)
						}%.`
					);
					break;
				}
				let vol = parseFloat(args[1]);
				if (isNaN(vol)) {
					reply(message, "Volume level must be a number.");
					break;
				}
				if (vol < 0 || vol > 100) {
					reply(message, "Volume must be between 0 and 100%.");
					break;
				}
				client.voiceConnection.setVolume(vol / 100);
				reply(message, `Volume set to ${vol.toFixed(2)}%.`);
				break;
			case "nowplaying":
			case "np":
				if (!nowPlaying) {
					reply(message, "There is nothing playing.");
					break;
				}
				reply(
					message,
					`Now playing: ${videoTitleWithLink(nowPlaying.info)}<br>` +
						`${nowPlaying.info.videoDetails.viewCount} views, ${nowPlaying.info.videoDetails.likes} likes.<br>` +
						`By <a href="${
							nowPlaying.info.videoDetails.author.channel_url
						}">${escapeHtml(
							nowPlaying.info.videoDetails.author.name
						)}</a><br>` +
						`Uploaded ${nowPlaying.info.videoDetails.uploadDate}<br>` +
						`${escapeHtml(
							nowPlaying.info.videoDetails.description.substring(
								0,
								100
							)
						).replace(/\n/g, "<br>")}${
							nowPlaying.info.videoDetails.description.length >
							100
								? "..."
								: ""
						}`
				);
				break;
			case "queue":
			case "q":
				if (videoQueue.length === 0) {
					reply(message, "The queue is empty.");
					break;
				}
				let queue = nowPlaying
					? `[NP] ${videoTitleWithLink(nowPlaying.info)}`
					: "";
				for (let i in videoQueue) {
					let video = videoQueue[i];
					queue += `[${i + 1}] ${videoTitleWithLink(video)}<br>`;
				}
				reply(message, queue);
				break;
			case "loop":
				loop = !loop;
				reply(message, "Looping is " + (looping ? "on" : "off") + ".");
				break;
			case "leave":
				if (nowPlaying) nowPlaying.stream.destroy();
				client.destroy();
				state.channel = null;
				break;
			case "ping":
				reply(message, "Pong!");
				break;
		}
	}
});

let playing = false;
/**
 * @typedef {Object} nowPlaying
 * @property {ytdl.videoInfo} info
 * @property {ReadableStream} stream
 */
/** @type {nowPlaying} */
let nowPlaying = null;
let loop = false;
function playNext() {
	if (videoQueue.length > 0) {
		let video = loop ? videoQueue[0] : videoQueue.shift();
		client.sendMessage(`Now playing: ${videoTitleWithLink(video)}`);
		let audioStream = ytdl.downloadFromInfo(video, {
			format: ytdl.chooseFormat(video.formats, {
				quality: "highestaudio",
				filter: "audioonly"
			})
		});
		audioStream.on("close", playNext);
		if (nowPlaying) nowPlaying.stream.destroy();
		client.voiceConnection.stopStream();
		client.voiceConnection.play(audioStream);
		playing = true;
		nowPlaying = {
			info: video,
			stream: audioStream
		};
	} else (playing = false), (nowPlaying = null);
}

client.connect();
