const { existsSync, writeFileSync, readFileSync } = require("fs");

module.exports = (filename, value) =>
	new Proxy(
		existsSync(filename) ? JSON.parse(readFileSync(filename)) : value,
		{
			set: (target, key, value) => {
				Reflect.set(target, key, value);
				writeFileSync(filename, JSON.stringify(target));
				return true;
			}
		}
	);
